colorama==0.4.4
colorlog==4.0.2
requests==2.26.*

bitbucket_pipes_toolkit==3.2.1
