import os

import pytest

from bitbucket_pipes_toolkit.test import PipeTestCase


class TriggerBuildBaseTestCase(PipeTestCase):

    def run_container(self, *args, **kwargs):
        kwargs['environment'].update(
            {'BITBUCKET_REPO_OWNER': os.getenv('BITBUCKET_USERNAME'),
             'ACCOUNT': 'bbcitest'}
        )
        return super().run_container(*args, **kwargs)


class TriggerBuildTestCase(TriggerBuildBaseTestCase):

    def test_build_successfully_started_master(self):
        result = self.run_container(environment={
            'BITBUCKET_USERNAME': os.getenv('BITBUCKET_USERNAME'),
            'BITBUCKET_APP_PASSWORD': os.getenv('BITBUCKET_APP_PASSWORD'),
            'REPOSITORY': os.getenv('REPOSITORY', 'test-trigger-build'),
            'WAIT': 'true'
        })

        self.assertRegexpMatches(
            result, r'✔ Build number \d+ for [-\w]+@master finished successfully')

    @pytest.mark.skip(reason='Skip until we add tags support back')
    def test_build_successfull_for_tag(self):
        tagname = '1.0.0'
        result = self.run_container(environment={
            'BITBUCKET_USERNAME': os.getenv('BITBUCKET_USERNAME'),
            'BITBUCKET_APP_PASSWORD': os.getenv('BITBUCKET_APP_PASSWORD'),
            'REPOSITORY': os.getenv('REPOSITORY', 'test-trigger-build'),
            'WAIT': 'true',
            'REF_TYPE': 'tag',
            'BRANCH_NAME': tagname
        })

        self.assertRegexpMatches(
            result, rf'✔ Build number \d+ for [-\w]+@{tagname} finished successfully')

    @pytest.mark.skip(reason='Skip until we add tags support back')
    def test_build_fails_for_wrong_tag(self):
        tagname = 'no-such-tag'
        result = self.run_container(environment={
            'BITBUCKET_USERNAME': os.getenv('BITBUCKET_USERNAME'),
            'BITBUCKET_APP_PASSWORD': os.getenv('BITBUCKET_APP_PASSWORD'),
            'REPOSITORY': os.getenv('REPOSITORY', 'test-trigger-build'),
            'WAIT': 'true',
            'REF_TYPE': 'tag',
            'BRANCH_NAME': tagname
        })

        self.assertRegexpMatches(
            result, rf'✖ Failed to get the tag info for {tagname}')

    def test_build_should_fail_if_downstream_fails(self):
        ref = 'always-fail'
        result = self.run_container(environment={
            'BITBUCKET_USERNAME': os.getenv('BITBUCKET_USERNAME'),
            'BITBUCKET_APP_PASSWORD': os.getenv('BITBUCKET_APP_PASSWORD'),
            'REPOSITORY': os.getenv('REPOSITORY', 'test-trigger-build'),
            'BRANCH_NAME': ref,
            'WAIT': 'true'
        })

        self.assertRegexpMatches(
            result, rf'✖ Build number \d+ for [-\w]+@{ref} failed')

    def test_pipe_should_timeout(self):
        ref = 'always-fail'
        result = self.run_container(environment={
            'BITBUCKET_USERNAME': os.getenv('BITBUCKET_USERNAME'),
            'BITBUCKET_APP_PASSWORD': os.getenv('BITBUCKET_APP_PASSWORD'),
            'REPOSITORY': os.getenv('REPOSITORY', 'test-trigger-build'),
            'BRANCH_NAME': ref,
            'WAIT': 'true',
            'WAIT_MAX_TIMEOUT': 1
        })

        self.assertRegexpMatches(
            result, '✖ Timeout waiting for the pipeline completion')

    def test_different_workspace(self):
        result = self.run_container(environment={
            'BITBUCKET_USERNAME': os.getenv('BITBUCKET_USERNAME'),
            'ACCOUNT': 'bitbucketpipelines',
            'BITBUCKET_APP_PASSWORD': os.getenv('BITBUCKET_APP_PASSWORD'),
            'REPOSITORY': os.getenv('REPO', 'test-trigger-build'),
            'BRANCH_NAME': 'master',
            'WAIT': 'true',
        })

        self.assertRegexpMatches(
            result, rf'✔ Build number \d+ for [-\w]+@master finished successfully')

    def test_build_success_if_manual_step(self):
        result = self.run_container(environment={
            'BITBUCKET_USERNAME': os.getenv('BITBUCKET_USERNAME'),
            'ACCOUNT': 'bbcitest',
            'BITBUCKET_APP_PASSWORD': os.getenv('BITBUCKET_APP_PASSWORD'),
            'REPOSITORY': 'test-trigger-build',
            'BRANCH_NAME': 'develop',
            'WAIT': 'true',
        })

        self.assertRegexpMatches(
            result, rf'✔ Build number \d+ for [-\w]+@develop is paused')


class InvalidParametersTestCase(TriggerBuildBaseTestCase):

    def test_no_parameters(self):
        result = self.run_container(environment={})
        self.assertIn('BITBUCKET_APP_PASSWORD:\n- required field', result)

    def test_no_repo(self):
        result = self.run_container(environment={})
        self.assertIn('REPOSITORY:\n- required field', result)

    def test_pipe_fails_for_nonexistent_branch(self):
        result = self.run_container(environment={
            'BITBUCKET_USERNAME': os.getenv('BITBUCKET_USERNAME'),
            'BITBUCKET_APP_PASSWORD': os.getenv('BITBUCKET_APP_PASSWORD'),
            'REPOSITORY': os.getenv('REPOSITORY', 'test-trigger-build'),
            'BRANCH_NAME': 'no-such-branch',
        })

        self.assertRegexpMatches(
            result, r"✖ Account, repository or branch doesn't exist")

    def test_pipe_wait_should_be_boolean(self):
        result = self.run_container(environment={
            'WAIT': 'not-a-bool',
        })

        self.assertRegexpMatches(
            result, r"WAIT:\n- must be of boolean type")

    def test_pipe_should_fail_wrong_account(self):
        result = self.run_container(environment={
            'BITBUCKET_USERNAME': 'NoSuchAccount',
            'BITBUCKET_APP_PASSWORD': os.getenv('BITBUCKET_APP_PASSWORD'),
            'REPOSITORY': os.getenv('REPOSITORY', 'test-trigger-build'),
            'BRANCH_NAME': 'no-such-branch',
        })

        self.assertRegexpMatches(
            result, r"✖ API request failed with status 401")

    def test_pipe_should_fail_wrong_repo(self):
        result = self.run_container(environment={
            'BITBUCKET_USERNAME': os.getenv('BITBUCKET_USERNAME'),
            'BITBUCKET_APP_PASSWORD': os.getenv('BITBUCKET_APP_PASSWORD'),
            'REPOSITORY': 'no-such-repo-hopefully',
            'BRANCH_NAME': 'no-such-branch',
        })

        self.assertRegexpMatches(
            result, r"✖ Account, repository or branch doesn't exist")


class PipelineTypeTestCase(TriggerBuildBaseTestCase):

    def test_custom_with_pattern(self):
        result = self.run_container(environment={
            'BITBUCKET_USERNAME': os.getenv('BITBUCKET_USERNAME'),
            'BITBUCKET_APP_PASSWORD': os.getenv('BITBUCKET_APP_PASSWORD'),
            'REPOSITORY': 'test-trigger-build',
            'BRANCH_NAME': 'custom-pipeline',
            'CUSTOM_PIPELINE_NAME': 'test-custom',
            'WAIT': 'true'
        })

        self.assertRegexpMatches(
            result, r"Build number \d+ for [-\w]+@custom-pipeline finished successfully")


class PipelineVariablesTestCase(TriggerBuildBaseTestCase):

    def test_custom_pipeline_with_variables_should_run(self):
        """
        The downstream pipeline will fail if the MY_VARIABLE is not set.
        """
        variables = """
        [{
            "key": "MY_VARIABLE",
            "value": "$BITBUCKET_BUILD_NUMBER"
          }, {
            "key": "var2key",
            "value": "var2value"
        }]
        """
        result = self.run_container(environment={
            'BITBUCKET_USERNAME': os.getenv('BITBUCKET_USERNAME'),
            'BITBUCKET_APP_PASSWORD': os.getenv('BITBUCKET_APP_PASSWORD'),
            'REPOSITORY': 'test-trigger-build',
            'BRANCH_NAME': 'master',
            'CUSTOM_PIPELINE_NAME': 'test-custom-variables',
            'WAIT': 'true',
            'PIPELINE_VARIABLES': variables
        })

        self.assertRegexpMatches(
            result, r"Build number \d+ for [-\w]+@master finished successfull")


class RegressionSpecialCharsTestCase(TriggerBuildBaseTestCase):

    def test_special_chars_in_app_password(self):
        """
        The downstream pipeline will fail if the MY_VARIABLE is not set.
        """
        result = self.run_container(environment={
            'BITBUCKET_USERNAME': os.getenv('BITBUCKET_USERNAME'),
            'BITBUCKET_APP_PASSWORD': os.getenv('BITBUCKET_WRONG_PASSWORD', '%!asds$dad'),
            'REPOSITORY': os.getenv('REPOSITORY', 'test-trigger-build'),
            'WAIT': 'true'
        })

        self.assertRegexpMatches(
            result, r"Check your account and app password and try again")
