# Bitbucket Pipelines pipe: trigger Bitbucket pipeline

Trigger a pipeline in a Bitbucket repository. You have the ability to specify a branch and the pipeline to run for the branch. You can also configure this pipe to wait until the triggered pipeline has finished and reported its status. See the **Examples** section for how to configure this pipe.

## YAML Definition

Add the following snippet to the script section of your `bitbucket-pipelines.yml` file:

```yaml
script:
  - pipe: atlassian/trigger-pipeline:4.3.1
    variables:
      BITBUCKET_USERNAME: '<string>'
      BITBUCKET_APP_PASSWORD: '<string>'
      REPOSITORY: '<string>'
      # ACCOUNT: '<string>' # Optional
      # BRANCH_NAME: '<string>' # Optional
      # CUSTOM_PIPELINE_NAME: '<string>' # Optional
      # PIPELINE_VARIABLES: '<json>' # Optional
      # WAIT: '<boolean>' # Optional
      # WAIT_MAX_TIMEOUT: '<string>' # Optional
      # DEBUG: '<boolean>' # Optional
```
## Variables

| Variable              | Usage                                                       |
| --------------------- | ----------------------------------------------------------- |
| BITBUCKET_USERNAME  (*) | Bitbucket user that will trigger the pipeline. Note, that this should be an account name, not the email. |
| BITBUCKET_APP_PASSWORD (*)      | [Bitbucket app password](https://confluence.atlassian.com/bitbucket/app-passwords-828781300.html) of the user that will trigger the pipeline. |
| REPOSITORY (*)              | Name of the Bitbucket repository. |
| ACCOUNT             | The account or team name of the Bitbucket repository in which you want to trigger a pipeline. Default `$BITBUCKET_REPO_OWNER`. |
| BRANCH_NAME        | The name of the branch to run the pipeline on. Default `master`. |
| CUSTOM_PIPELINE_NAME     | The name of the custom pipeline to run for a branch. Default: `null` and will run the pipeline defined for the branch. If custom pipeline name is provided, the custom pipeline will be run for for the branch.|
| PIPELINE_VARIABLES    | JSON document containing variables to pass to the pipeline you want to trigger. This currently only works for `custom` piplines. The value should be a list of object with `key`, `value` and `secure` fields for each of your variables. The **Examples** section below contains an example for passing variables to the custom pipeline. |
| WAIT                  | Flag to wait until the triggered build finishes. Default: `false`. If set to `true`, this pipe will wait for the triggered pipeline to complete. If the triggered pipeline fails, this pipe will also fail. |
| WAIT_MAX_TIMEOUT      | Maximum time in seconds to wait for the triggered build to complete. Default: `3600`. Pipe will fail if the timeout is reached.|
| DEBUG                 | Flag to turn on extra debug information. Default: `false`. |

_(*) = required variable._

## Prerequisites

To use this pipe, you need to [generate an app password](https://confluence.atlassian.com/bitbucket/app-passwords-828781300.html). Remember to check the `Pipelines Write` and `Repositories Read` permissions when generating the app password. If you want to trigger a pipeline in a repository owned by a team account, make sure you have the correct access to the repository.

## Examples

Basic example. This pipe will trigger the branch pipeline for `master` in `your-awesome-repo`. This pipeline will continue, without waiting for the triggered pipeline to complete. 

```yaml
script:
  - pipe: atlassian/trigger-pipeline:4.3.1
    variables:
      BITBUCKET_USERNAME: $BITBUCKET_USERNAME
      BITBUCKET_APP_PASSWORD: $BITBUCKET_APP_PASSWORD
      REPOSITORY: 'your-awesome-repo'
```

Basic example. This pipe will trigger the branch pipeline for `master` in `your-awesome-repo` that is owned by the `teams-in-space` Bitbucket account.

```yaml
script:
  - pipe: atlassian/trigger-pipeline:4.3.1
    variables:
      BITBUCKET_USERNAME: $BITBUCKET_USERNAME
      BITBUCKET_APP_PASSWORD: $BITBUCKET_APP_PASSWORD
      REPOSITORY: 'your-awesome-repo'
      ACCOUNT: 'teams-in-space'
```

Advanced example. This pipe will trigger the branch pipeline for `develop` in `your-awesome-repo`. This pipeline will wait for the triggered pipeline to complete and will return its status. If the triggered pipeline fails, this pipeline will also fail.

_Note! Make sure that you have such branch in your repository to trigger it. Also, setup pipeline for this branch or have a default pipeline._

```yaml
script:
  - pipe: atlassian/trigger-pipeline:4.3.1
    variables:
      BITBUCKET_USERNAME: $BITBUCKET_USERNAME
      BITBUCKET_APP_PASSWORD: $BITBUCKET_APP_PASSWORD
      REPOSITORY: 'your-awesome-repo'
      BRANCH_NAME: 'develop'
      WAIT: 'true'
```

Advanced example. This pipe will trigger the `deployment-pipeline` for `master` in `your-awesome-repo`. This pipe passes 3 variables to the triggered pipeline, which performs a deployment to AWS, using JSON.
```yaml
script:
  - pipe: atlassian/trigger-pipeline:4.3.1
    variables:
      BITBUCKET_USERNAME: $BITBUCKET_USERNAME
      BITBUCKET_APP_PASSWORD: $BITBUCKET_APP_PASSWORD
      REPOSITORY: 'your-awesome-repo'
      BRANCH_NAME: 'master'
      CUSTOM_PIPELINE_NAME: 'deployment-pipeline'
      PIPELINE_VARIABLES: >
          [{
            "key": "AWS_DEFAULT_REGION",
            "value": "us-west-1"
          },
          {
            "key": "AWS_ACCESS_KEY_ID",
            "value": "$AWS_ACCESS_KEY_ID",
            "secured": true
          },
          {
            "key": "AWS_SECRET_ACCESS_KEY",
            "value": "$AWS_SECRET_ACCESS_KEY",
            "secured": true
          }]
      WAIT: 'true'

```
    
## Support
If you'd like help with this pipe, or you have an issue or feature request, [let us know on the Atlassian Community][community].

If you’re reporting an issue, please include:

- the version of the pipe
- relevant logs and error messages
- steps to reproduce

## License
Copyright (c) 2018 Atlassian and others.
Apache 2.0 licensed, see [LICENSE.txt](LICENSE.txt) file.


[community]: https://community.atlassian.com/t5/forums/postpage/board-id/bitbucket-pipelines-questions?add-tags=pipes,trigger
